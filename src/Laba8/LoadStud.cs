﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class LoadStud
    {
        public static Container<Student> students = ReadFile();

        public static Container<Student> ReadFile()
        {
            Container<Student> temp = new Container<Student>();
            string LastName;
            string FirstName;
            string Fathersname;
            DateTime Birthday;
            DateTime DateEnter;
            string NameOfGroup;
            char Index_of_group;
            string Specialization;
            double _perform;

            string path = @"C:\Users\Lenovo\Desktop\test.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split("/");

                        LastName = infoStudent[0];
                        FirstName = infoStudent[1];
                        Fathersname = infoStudent[2];
                        Birthday = DateTime.Parse(infoStudent[3]);
                        DateEnter = DateTime.Parse(infoStudent[4]);
                        NameOfGroup = infoStudent[5];
                        Index_of_group = char.Parse(infoStudent[6]);
                        Specialization = infoStudent[7];
                        _perform = int.Parse(infoStudent[8]);
                        var obj = new Student(NameOfGroup, Index_of_group, Specialization, FirstName, LastName, Fathersname, Birthday, DateEnter, _perform);
                        temp.Add(obj);
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return temp;
        }
    }
}
