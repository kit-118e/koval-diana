﻿using System;

namespace Koval01
{
    public static class Menu
    {
        public static void Start()
        {
            var list = new Container();
            char choice = 'a';
            
            Student tmpStudent;
            //int tmpIndex;

            while (choice != '0')
            {
                printMenu();
                Console.Write("Your choice: ");
                choice = Entry.InputChar();

                switch (choice)
                {
                    case '1':
                        tmpStudent = Entry.InputStudent();
                        list.AddStudent(tmpStudent);
                        break;
                    case '2':
                        list.ShowAll();
                        break;
                }
            }
        }

        private static void printMenu()
        {
            Console.WriteLine("------Menu------\n");
            Console.WriteLine("1 - Add new student");
            Console.WriteLine("2 - Show data for all students");
            Console.WriteLine("0 - Exit");
        }
    }
}
